import React, { Component } from 'react';
import './card.component.css';

class Card extends Component {
  render() {
    return (
      <div className="container shadow auth-card clear-padding">
        <div className="card-container">
          <div className="col-md-5 col-xs-12 left-side">
            <div className="card-content">
              <div className="row content-area">
                <img src="logo.png" alt=""/>
              </div>
              <div className="row text">Join us & start</div>
              <div className="row text">your campaign</div>
            </div>
          </div>
          <div className="col-md-7 col-xs-12 right-side">
            <div className="card-content">
              <div className="row content-area">
                <button className="btn btn-auth btn-facebook">
                  <i className="ti-facebook"></i>
                  Join with Facebook
                </button>
              </div>
              <div className="row content-area">
                <button className="btn btn-auth btn-google">
                  <i className="ti-google"></i>
                  Join with Google
                </button>
              </div>
              <div className="row">
                <button className="btn btn-auth btn-email">
                  Join with Email
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Card;
