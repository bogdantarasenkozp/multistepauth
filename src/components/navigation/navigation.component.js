import React, { Component }     from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import './navigation.component.css';

class Navigation extends Component {

  render() {
    return (
        <Navbar fluid inverse collapseOnSelect className="navbar navbar-default">
          <div className="nav-container">
            <Navbar.Header className="navbar-header">
              <Navbar.Brand className="navbar-brand vertical-center">
                <a href="#">
                  <img className="nav-logo" src="logo.png" alt=""/>
                </a>
              </Navbar.Brand>
              <Navbar.Toggle className="navbar-toggle" />
            </Navbar.Header>

            <Navbar.Collapse>
              <Nav className="nav navbar-nav navbar-right vertical-center">
                <NavItem eventKey={1} href="#">
                  <a href="#" className="btn btn-simple">Log in</a>
                </NavItem>
               </Nav >
            </Navbar.Collapse>
          </div>
        </Navbar>
    )
  }
}

export default Navigation;
