import React, { Component } from 'react';
import { Navigation, Card } from './../index';
import './app.component.css';

class App extends Component {
  render() {
    return (
      <div className="app">
				<Navigation />
        <Card />
			</div>
    );
  }
}

export default App;
